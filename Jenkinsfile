pipeline {
 
  agent {
    kubernetes {
      label 'hugo-agent'
      yaml """
apiVersion: v1
metadata:
  labels:
    run: hugo
  name: hugo-pod
spec:
  containers:
    - name: jnlp
      volumeMounts:
      - mountPath: /home/jenkins/agent/.ssh
        name: volume-known-hosts
      env:
      - name: "HOME"
        value: "/home/jenkins/agent"
    - name: hugo
      image: eclipsecbi/hugo:0.42.1
      command:
      - cat
      tty: true
  volumes:
  - configMap:
      name: known-hosts
    name: volume-known-hosts
"""
    }
  }
 
  environment {
    GROUP_NAME = "app4mc" // GitLab group
    PROJECT_NAME = "org.eclipse.app4mc.website" // GitLab target project
    PROJECT_BOT_NAME = "APP4MC Bot" // Capitalize the name
    PROJECT_BOT_EMAIL = "app4mc-bot@eclipse.org" // Do not change

    SOURCE_PATH = "websites/www.eclipse.org-app4mc-hugo/site"
    TARGET_REPO = "git@gitlab.eclipse.org:eclipse/${GROUP_NAME}/${PROJECT_NAME}.git"
    TARGET_ADDRESS = "https://eclipse.dev/app4mc/"
  }
 
  options {
    buildDiscarder(logRotator(numToKeepStr: '5'))
    checkoutToSubdirectory('hugo')
    disableConcurrentBuilds()
  }
 
  stages {
    stage('Checkout www repo') {
      steps {
        dir('www') {
            sshagent(['gitlab-bot-ssh']) {
                sh '''
                    git clone ${TARGET_REPO} .
                    git checkout ${BRANCH_NAME}
                '''
            }
        }
      }
    }
    stage('Build website (master) with Hugo') {
      when {
        branch 'master'
      }
      steps {
        container('hugo') {
            dir("hugo/${SOURCE_PATH}") {
                sh 'hugo -b ${TARGET_ADDRESS}'
            }
        }
      }
    }
    stage('Build website (staging) with Hugo') {
      when {
        branch 'staging'
      }
      steps {
        container('hugo') {
            dir("hugo/${SOURCE_PATH}") {
                sh 'hugo -b https://staging.eclipse.org/app4mc/'
            }
        }
      }
    }
    stage('Push to $env.BRANCH_NAME branch') {
      when {
        anyOf {
          branch "master"
          branch "staging"
        }
      }
      steps {
        sh 'rm -rf www/* && cp -Rvf hugo/${SOURCE_PATH}/public/* www/'
        dir('www') {
            sshagent(['gitlab-bot-ssh']) {
                sh '''
                git add -A
                if ! git diff --cached --exit-code; then
                  echo "Changes have been detected, publishing to repo '${GROUP_NAME}/${PROJECT_NAME}'"
                  git config --global user.email "${PROJECT_BOT_EMAIL}"
                  git config --global user.name "${PROJECT_BOT_NAME}"
                  git commit -m "Website build ${JOB_NAME}-${BUILD_NUMBER}"
                  git log --graph --abbrev-commit --date=relative -n 5
                  git push origin HEAD:${BRANCH_NAME}
                else
                  echo "No change have been detected since last build, nothing to publish"
                fi
                '''
            }
        }
      }
    }
  }
}

