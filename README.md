
# Eclipse APP4MC - Website sources

This repository contains sources for the Eclipse APP4MC Websites:

    websites\www.eclipse.org-app4mc-hugo  |    Hugo sources of site https://eclipse.dev/app4mc/


## Build

Changes in the repository automatically trigger the site generation (and the copying to the web server).

## Authoring

* Take a look  at the HUGO [Website][Hugo Site]
* Edit the Markdown files below 'websites\www.eclipse.org-app4mc-hugo\site\content'
* Get a **0.42.x** version of Hugo from [Github][Hugo Release]
* Start hugo in folder 'websites\www.eclipse.org-app4mc-hugo\site'

Build draft (-D) and future (-F) pages and provide them via an embedded web server:
```
$ hugo.exe -DF server
```

## License

[Eclipse Public License (EPL) v2.0][EPL]

[EPL]: https://www.eclipse.org/legal/epl-2.0/
[Hugo Site]: https://gohugo.io
[Hugo Release]: https://github.com/gohugoio/hugo/releases/tag/v0.42.2
