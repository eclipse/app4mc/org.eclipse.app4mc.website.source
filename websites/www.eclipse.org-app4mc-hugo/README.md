# APP4MC project website

This is the [Hugo](http://gohugo.io/) based development and build environment to create a static webpage for the Eclipse APP4MC project. The build environment enables to create content and adjust the styling of the website, and it includes a Grunt based deployment job creating the subdirectory build/dev that contains all the necessary files to deploy the website. The work is based on Christopher Guidon's [Solstice Hugo Starter Kit](git@github.com:EclipseFdn/solstice-hugo-starterkit.git)


## Requirements
* [Hugo](http://gohugo.io/) version >= v0.42
* [Solstice Hugo Starter Kit](git@github.com:EclipseFdn/solstice-hugo-starterkit.git)

## Prerequisites contained in the Git repository

Bootstrap, Fontawesome, Yamm3

### What is Hugo?

[Hugo](http://gohugo.io/) is a static site generator. This means that, unlike systems like WordPress, Ghost and Drupal, which run on your web server expensively building a page every time a visitor requests one, Hugo does the building when you create your content.

#### How to install

Please refer to the [Installing Hugo](http://gohugo.io/overview/installing/) documentation.

## Developing

```shell
cd site
hugo or
hugo server --watch
```

See the results at
The edit task includes a local server with live reload on http://127.0.0.1:8080.

