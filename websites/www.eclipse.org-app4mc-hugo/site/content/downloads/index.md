---
date: 2024-07-15
title: "Downloads"
weight: 5
hide_sidebar: "true"
---


---
### **Release 3.3.0**
##### 2024-07-15 &nbsp;&nbsp; - &nbsp;&nbsp; based on Eclipse 2023-03
---

Minimum Java runtime is **Java 17**.

#### Products

* APP4MC Platform 3.3.0 for **Linux (64 Bit)** - [Zip-archive](https://www.eclipse.org/downloads/download.php?file=/app4mc/products/releases/3.3.0/org.eclipse.app4mc.platform-3.3.0-20240715-102235-linux.gtk.x86_64.tar.gz) (410 MB)

* APP4MC Platform 3.3.0 for **OSX (64 Bit)** - [Zip-archive](https://www.eclipse.org/downloads/download.php?file=/app4mc/products/releases/3.3.0/org.eclipse.app4mc.platform-3.3.0-20240715-102235-macosx.cocoa.x86_64.dmg) (407 MB)

* APP4MC Platform 3.3.0 for **Windows (64 Bit)** - [Zip-archive](https://www.eclipse.org/downloads/download.php?file=/app4mc/products/releases/3.3.0/org.eclipse.app4mc.platform-3.3.0-20240715-102235-win32.win32.x86_64.zip) (404 MB)

#### Update Site

* direct link: https://download.eclipse.org/app4mc/updatesites/releases/3.3.0/

* for offline installation: [Zip-archive of APP4MC 3.3.0 p2 repository](https://www.eclipse.org/downloads/download.php?file=/app4mc/products/releases/3.3.0/org.eclipse.app4mc.p2repo-3.3.0.zip)

---

_For older versions please refer to:_

* [Eclipse APP4MC project page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
