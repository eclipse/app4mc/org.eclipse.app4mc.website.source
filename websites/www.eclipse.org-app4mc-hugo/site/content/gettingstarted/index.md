---
date: 2022-07-12
title: "Getting Started"
hide_sidebar: "false"
---

This tutorial will get you started with APP4MC. It is based on APP4MC version 2.2.0.

---
#### Tutorial 1 - Create an AMALTHEA model
--- 
The purpose of this tutorial is to get familiar with the structure
of the AMALTHEA model itself as well as the currently available editor.  
It contains step by step directions to create a project, a model folder
and several model files.  
[start tutorial](../gs-create-model)

