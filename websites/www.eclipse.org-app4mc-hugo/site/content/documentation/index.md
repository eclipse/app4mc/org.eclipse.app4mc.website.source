---
title: "Documentation"
date: 2024-07-15
hide_sidebar: "false"
---


Sign up on the [APP4MC mailing list](https://dev.eclipse.org/mailman/listinfo/app4mc-dev) for the latest news about APP4MC.

---
### Help
---

#### **APP4MC**

<a href="/help/latest/index.html" target="_blank">APP4MC 3.3 - Online Help</a> (opens in new tab)

[APP4MC 3.3 - Help](https://archive.eclipse.org/app4mc/documents/help/app4mc-3.3-help.zip) (zip archive)

<br>
#### Previous versions (zip archive only):

[Help Archive](https://archive.eclipse.org/app4mc/documents/help)  

---
#### **APP4MC.sim**

[APP4MC.sim - Online Help](../doc-app4mc.sim)

<br>

---
### Related Standards
---

#### **BTF Specification**
Current version: [V2.2.1 (2021-01-07)](https://archive.eclipse.org/app4mc/documents/misc/BTF_Specification_2.2.1.pdf)

Previous versions:
[V2.1.5 (2016-01-29)](https://archive.eclipse.org/app4mc/documents/misc/BTF_Specification_2.1.5.pdf)
