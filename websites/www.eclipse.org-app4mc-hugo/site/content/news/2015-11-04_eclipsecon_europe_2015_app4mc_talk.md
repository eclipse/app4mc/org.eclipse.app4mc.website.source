---
date: 2015-11-04
title: APP4MC Talk EclipseCon Europe 2015
slug: "2015-11-04_eclipsecon_europe_2015_app4mc_talk"
categories: ["news"]
---

At EclipseCon Europe 2015, Ludwigsburg - APP4MC - support for embedded multi-core

<!--more-->

At EclipseCon Europe 2015, Ludwigsburg - [APP4MC - support for embedded multi-core](https://www.eclipsecon.org/europe2015/session/app4mc-support-embedded-multicore)  
_Presentation by **Harald Mackamul**, Bosch_

View the video of Harald Mackamul's presentation, [APP4MC - support for embedded multi-core](https://youtu.be/2bkCh1sh_aQ) on youtube. You can also [read the abstract](https://www.ecli
psecon.org/europe2015/session/app4mc-support-embedded-multicore)
<!--more-->
