---
date: 2016-09-28
title: AMALTHEA4public - 2nd annual review passed
slug: "2016-09-28_itea-review"
categories: ["news"]
---

The 2nd annual project review held in Bilbao is passed. 
The project itself presented the work of the last year to show the outcome and usage of the AMALTHEA data model and the APP4MC platform. 

<!--more-->

__AMALTHEA4public__

Overall the results and feedback of the reviewers were positive. The expectation for the last period of the project is the broader usage and an increasing involvement of outside partners. 

To find out more:

* Visit the [ITEA project page](https://itea3.org/project/amalthea4public.html)
