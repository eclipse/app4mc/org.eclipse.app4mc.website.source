---
date: 2023-06-30
title: "APP4MC - Release 3.1.0 published"
categories: ["news"]
slug: "2023-06-30-release-3-1-0"
---

We released a new version of APP4MC with new features and improvements.

<!--more-->

__Extract of APP4MC 3.1.0 Release notes__


Model handling

	Model migration support (3.0.0 -> 3.1.0)


Model

    No model changes


Product

    Updated frameworks: Visualization, Editor contribution
    New visualizations: Tag members, Event chains
    Updated visualization: Hardware structure
    IDE examples included in the standard distribution
    Several bug fixes

Minimum Java runtime is **Java 11**.  

__Developer info__

* Amalthea components (model, validation, migration) are available at **[Maven Central](https://search.maven.org/search?q=app4mc)**

__Further details__

* See slides [New & Noteworthy](https://archive.eclipse.org/app4mc/documents/news/APP4MC_3.1.0_New_and_noteworthy.pdf)
* Visit the [APP4MC download page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
