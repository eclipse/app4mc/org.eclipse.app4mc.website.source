---
date: 2017-10-30
title: APP4MC - Release 0.8.2 published
slug: "2017-10-30_release_0-8-2"
categories: ["news"]
---

We released a new version of APP4MC with a couple of new features and improvements.

<!--more-->

__Extract of APP4MC 0.8.2__

General

    Model changes (extensions)
    New graphical editors (based on Sirius)

Model handling

    Model migration support (0.8.1 -> 0.8.2)
	New model utilities
    
Model changes / Improvements

    New runnable item to get the result of an asynchronous request
    New possibility to specify the label access implementation
    New measurement model
    New component event

For details see [Release 0.8.2 issues] (https://projects.eclipse.org/projects/automotive.app4mc/releases/0.8.2/bugs)
* Visit the [APP4MC download page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
