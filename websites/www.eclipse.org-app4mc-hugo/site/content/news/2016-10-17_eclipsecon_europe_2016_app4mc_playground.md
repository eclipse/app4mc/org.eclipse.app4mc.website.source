---
date: 2016-10-17
title: Visit APP4MC at EclipseCon Europe 2016
slug: "2016-10-17_eclipsecon_europe_2016_app4mc_playground"
categories: ["news"]
---

Learn more about APP4MC and see it in action at EclipseCon Europe 2016.

<!--more-->

Learn more about APP4MC and see it in action at [EclipseCon Europe 2016](https://eclipsecon.org/europe2016/). Explore APP4MC through a demo of a radio-controlled car at the [IoT Playground](https://www.eclipsecon.org/europe2016/playground) on Wednesday. Start the week by meeting members of the project team at the 
[Unconference](https://wiki.eclipse.org/Eclipse_Unconference_Europe_2016#Unconference_Agenda).

__Unconference__

The APP4MC project team will be participating at this year's [Unconference](https://wiki.eclipse.org/Eclipse_Unconference_Europe_2016#Unconference_Agenda) as part of a meeting of two new Working Groups in the automotive domain, OpenMDM and OpenPASS. 

__APP4MC in the IoT Playground__

Drop by the [IoT Playground](https://www.eclipsecon.org/europe2016/playground) on Wednesday to explore the potential of the APP4MC platform when used to control a radio-controlled car. Implemented features include visual traffic cone detection and ultrasonic sensor-based obstacle detection. The platform creates models with various configurations and timing behaviors that can be observed and investigated, as well as selected and combined into different features of the car. 