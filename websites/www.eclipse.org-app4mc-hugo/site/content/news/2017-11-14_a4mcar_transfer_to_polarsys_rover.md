---
date: 2017-11-14
title: A4MCAR Technology Transfer to PolarSys Rover
slug: "2017-11-14_a4mcar_transfer_to_polarsys_rover"
categories: ["news"]
---

The A4MCAR project, an APP4MC demonstrator developed by the Dortmund University of Applied Sciences and Arts will contribute to the PolarSys Rover project. The A4MCAR features functionalities from sensor and motor driving to high level functionalities such as server-based wireless driving via a Web-UI and system core monitoring and analysis. Mustafa Ozcelikors describes the details of the ongoing work in a PolarSys interview.

<!--more-->

The A4MCAR project, an APP4MC demonstrator developed by the Dortmund University of Applied Sciences and Arts will contribute to the [PolarSys Rover project](https://www.polarsys.org/projects/polarsys.rover).  The A4MCAR  features functionalities from sensor and motor driving to high level functionalities such as server-based wireless driving via a Web-UI and system core monitoring and analysis. Mustafa Ozcelikors describes the details of the ongoing work in a [PolarSys interview](https://www.polarsys.org/polarsys-rover-receives-technology-transfer-a4mcar-project).