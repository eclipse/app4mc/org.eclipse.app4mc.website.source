---
date: 2015-06-11
title: APP4MC project proposal posted
slug: "2015-06-11_project_proposal_posted"
categories: ["news"]
---

This is the first step of creating an Eclipse project as described in the Eclipse Project Handbook.

<!--more-->

This is the first step of creating an Eclipse project as described in the [Eclipse Project Handbook](https://www.eclipse.org/projects/handbook/#starting).

Before project creation, the [proposal is available](https://projects.eclipse.org/proposals/app4mc) for community review and discussion for few weeks.

