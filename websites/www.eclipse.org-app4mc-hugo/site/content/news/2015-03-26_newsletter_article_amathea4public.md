---
date: 2015-03-26
title: Amathea4Public Article
slug: "2015-03-26_newsletter_article_amathea4public"
link: "https://dzone.com/articles/2018-java-ecosystem-executive-insights"
categories: ["news"]
---

_Article by **Christopher Brink and Jan Jatzkowski**, University of Paderborn published in the Eclipse newsletter_

<!--more-->

Last spring, Christopher Brink and Jan Jatzkowski [published an overview of the Amalthea4Public project](http://www.eclipse.org/community/eclipse_newsletter/2015/march/article2.php) and the roadmap that leads to the APP4MC project.

