---
date: 2023-08-22
menu:
  main:
    parent: "About"
title: History
weight: 5
hide_sidebar: "true"
---

<br>
## _Main development_

The development of the common data model and the tool platform happened mainly in 3 European ITEA projects.

---
### __AMALTHEA__

[**AMALTHEA**](https://itea4.org/project/amalthea.html) was a ITEA 2 funded European project
that developed an open and expandable tool platform for automotive embedded-system engineering
based on model-driven methodology.
Specific features include support for multi-core systems combined with AUTOSAR compatibility
and product-line engineering.
The resulting tool platform is distributed under an Eclipse public license.

<dl class="dl-horizontal">
  <dt>Period</dt>
  <dd>Jul 2011 - Apr 2014</dd>
  <dt>Call</dt>
  <dd>ITEA 2 Call 4</dd>
  <dt>Partners</dt>
  <dd>15</dd>
  <dt>Countries</dt>
  <dd><img src="/images/history/fi.png" width="15"> <a href="https://itea4.org/country/finland.html">Finland</a></dd>
  <dd><img src="/images/history/de.png" width="15"> <a href="https://itea4.org/country/germany.html">Germany</a></dd>
  <dd><img src="/images/history/tr.png" width="15"> <a href="https://itea4.org/country/turkiye.html">Türkiye</a></dd>
</dl>

---
### __AMALTHEA4public__

[**AMALTHEA4public**](https://itea4.org/project/amalthea4public.html) was a second ITEA 2 funded European project.
It has the goal to integrate the results of various publicly funded projects with new developments.
The intention is to position the open-source tool framework as a de-facto standard for future
software engineering design flows for automotive and other embedded systems.

Further info: [AMALTHEA/AMALTHEA4public website](http://www.amalthea-project.org/)

<dl class="dl-horizontal">
  <dt>Period</dt>
  <dd>Sep 2014 - Aug 2017</dd>
  <dt>Call</dt>
  <dd>ITEA 2 Call 8</dd>
  <dt>Partners</dt>
  <dd>20</dd>
  <dt>Countries</dt>
  <dd><img src="/images/history/de.png" width="15"> <a href="https://itea4.org/country/germany.html">Germany</a></dd>
  <dd><img src="/images/history/es.png" width="15"> <a href="https://itea4.org/country/spain.html">Spain</a></dd>
  <dd><img src="/images/history/se.png" width="15"> <a href="https://itea4.org/country/sweden.html">Sweden</a></dd>
  <dd><img src="/images/history/tr.png" width="15"> <a href="https://itea4.org/country/turkiye.html">Türkiye</a></dd>
</dl>

---
### __PANORAMA__

[**PANORAMA**](https://itea4.org/project/panorama.html) was a  ITEA 3 funded European project.
The goal was to research model-based methods and tools to master development of heterogeneous embedded hardware/software systems. The enhanced meta-model and the related tool framework is a common and open platform to support collaborative development.

Further info: [PANORAMA website](https://panorama-research.org/)

<dl class="dl-horizontal">
  <dt>Period</dt>
  <dd>Apr 2019 - Sep 2022</dd>
  <dt>Call</dt>
  <dd>ITEA 3 Call 4</dd>
  <dt>Partners</dt>
  <dd>23</dd>
  <dt>Countries</dt>
  <dd><img src="/images/history/fi.png" width="15"> <a href="https://itea4.org/country/finland.html">Finland</a></dd>
  <dd><img src="/images/history/de.png" width="15"> <a href="https://itea4.org/country/germany.html">Germany</a></dd>
  <dd><img src="/images/history/pt.png" width="15"> <a href="https://itea4.org/country/portugal.html">Portugal</a></dd>
  <dd><img src="/images/history/se.png" width="15"> <a href="https://itea4.org/country/sweden.html">Sweden</a></dd>
  <dd><img src="/images/history/tr.png" width="15"> <a href="https://itea4.org/country/turkiye.html">Türkiye</a></dd>
</dl>

<br><br>
## _Application_

The common data model and the tool platform was used and extended in several other publicly funded projects.

---
### __ARAMiS II__

ARAMiS II aimed at development processes, tools and platforms for the efficient use of multicore architectures available in industry.
The development process with its methods, tools and platform adaptations was evaluated in ten representative Use Cases from the application domains automotive, avionics, and industrial automation and corresponding demonstrators.  

Further info: [ARAMiS II website](https://www.aramis2.com)

<dl class="dl-horizontal">
  <dt>Period</dt>
  <dd>Oct 2016 - Sep 2019</dd>
  <dt>Partners</dt>
  <dd>32 (see: <a href="https://www.aramis2.com/partners/">consortium</a>)</dd>
  <dt>Countries</dt>
  <dd>Germany</dd>
</dl>

---
### __AMPERE__

[**AMPERE**](https://cordis.europa.eu/project/id/871669) was developing a new generation of software programming environments for low-energy and highly parallel and heterogeneous computing architectures.

The AMALTHEA data model was extended to handle different execution contexts and a code generation for the Robot Operating System (ROS2 / MicroROS) was added.

Further info: [AMPERE website](https://ampere-euproject.eu)

<dl class="dl-horizontal">
  <dt>Period</dt>
  <dd>Jan 2020 - Jun 2023</dd>
  <dt>Call</dt>
  <dd>Horizon 2020</dd>
  <dt>Partners</dt>
  <dd>9 (see: <a href="https://ampere-euproject.eu/about/partners">consortium</a>)</dd>
  <dt>Countries</dt>
  <dd>Czechia, France, Germany, Italy, Portugal, Spain, Switzerland</dd>
</dl>
